

# View site: https://clairecayol.gitlab.io/Research/



# A toolbox by a disease ecologist

This is very much an ongoing project. At the moment, this website describes the lab protocols used during my research. In the future, updates about my research will be added. 
One of the aims of this website project is to make the lab procedures that I have used, tested, observed or experimented available to my collaborators, people from my research group and helpers. The main goal is to improve the reproducibility and repeatability of my science.   

• Field: protocols used the field 

• Animal Facility: protocols used in animal facility

• Dirty lab: protocols used in animal facility and  dirty lab

• DNA: protocols used in DNA lab 

• Immunology: protocols of eco-immunology, ELISA 

• Safety and miscellanous: Safety procedures and other protocols

## Folder Usage

If you are a member of my previous ir current research group, you will find protocols under the Articles tab. If you want to use the protocols for your own experiments, the article to be quoted is cited in each protocol.

This project is version controlled via git hosted in GitLab to enable sharing and synchronization among collaborators.

In order to have a copy of this project in your computer, you must:

- Have access rights to the GitLab project repository at https://gitlab.com/ClaireCayol/Research
- Have git installed and set up in your computer. If you don't, go to  https://git-scm.com to download it.

If you never used git collaboratively, you will probably have to create an SSH key as described here.

To clone the full project repository, type in Terminal:

```{bash}
git clone git@gitlab.com:ClaireCayol/labprotocols.git
```
# The Theme used
The Theme used is: Creative Theme for Jekyll
A Jekyll implementation of the [Creative Theme](http://startbootstrap.com/template-overviews/creative/) template by [Start Bootstrap](http://startbootstrap.com). Creative is a one page Bootstrap theme for creatives, small businesses, and other multipurpose uses.

----

_Forked from https://gitlab.com/steko/test/_

[`.gitlab-ci.yml`]: https://gitlab.com/jekyll-themes/creative/blob/pages/.gitlab-ci.yml
[`_config.yml`]: https://gitlab.com/jekyll-themes/creative/blob/pages/_config.yml
[`Pages`]: https://gitlab.com/jekyll-themes/creative/tree/pages